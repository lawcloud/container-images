# Sources:
# https://github.com/devcontainers/images
# https://github.com/docker-library/php/tree/master/8.3/bookworm/apache
FROM mcr.microsoft.com/devcontainers/php:1-8.2-bookworm

# Fix default settings of xdebug to more sane default
RUN echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.mode = off" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.client_port = 9000" >> /usr/local/etc/php/conf.d/xdebug.ini

# Install MariaDB client
# Install libpng
RUN apt-get update \
    && export DEBIAN_FRONTEND=noninteractive \
    # Install libraries for php-extensions
    && apt-get install -y  \
        mariadb-client redis-tools \
        icu-devtools libavif-dev libghc-iconv-dev \
        git patch tini curl wget zip vim \
    # Install PHP extensions
    && curl -sSLf \
        -o /usr/local/bin/install-php-extensions \
        https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions \
    && chmod +x /usr/local/bin/install-php-extensions  \
    && install-php-extensions gd xdebug zip bcmath intl mailparse pcntl soap imagick pdo_mysql tidy \
    && rm /usr/local/bin/install-php-extensions \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# Add saner default for php
ADD devfs /

# Allow running composer install as root-user without complaining
ENV COMPOSER_ALLOW_SUPERUSER 1



# [Optional] Uncomment this section to install additional OS packages.
# RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
#     && apt-get -y install --no-install-recommends <your-package-list-here>

# [Optional] Uncomment this line to install global node packages.
# RUN su vscode -c "source /usr/local/share/nvm/nvm.sh && npm install -g <your-package-here>" 2>&1

