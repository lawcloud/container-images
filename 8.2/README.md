# Easily run the container 

```shell
podman build --no-cache -f Dockerfile -t i-php-82 . && \
    podman stop --ignore c-php-74 && \
    podman container rm --ignore c-php-82 && \
    podman run -dit --name c-php-82 i-php-82 && \
    podman exec c-php-82 rm -rf /app && \
    podman cp /home/int8/lawcloud-3 c-php-82:/app && \
    podman exec -it c-php-82 /bin/sh
```

# Run container if it is not staring
```shell

podman build --no-cache -f Dockerfile -t i-php-82 . && podman run --entrypoint /bin/sh -a --name c-php-82 i-php-82 

podman run --entrypoint /bin/sh -a --name c-php-82 i-php-82
```