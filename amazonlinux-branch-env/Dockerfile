# See https://docs.aws.amazon.com/linux/al2023/ug/what-is-amazon-linux.html
# See https://repost.aws/articles/ARM9q-NiODRKC9V7N_jJnNbg/how-do-i-compile-php8.2-extensions-on-amazon-linux-2023

FROM amazonlinux:latest as main

LABEL maintainer="Pieter Nys <pieter.nys@law.cloud>"

RUN dnf install -y --allowerasing \
        libicu git patch curl wget zip && \
    # Install php
    dnf install -y php8.2 && \
    # Install php-packages from repo
    dnf install -y php-gd php-zip php-bcmath php-intl php-pcntl php-pdo_mysql php-tidy && \
    # Instal pecl \
    dnf install -y php-devel php-pear gcc && \
    pecl update-channels && \
    # Install pecl-package xdebug
    pecl install xdebug && \
    echo -en 'zend_extension=xdebug.so\n' >> /etc/php.d/15-xdebug.ini && \
    # Install pecl-package imagick
    dnf install -y ImageMagick ImageMagick-devel && \
    pecl download Imagick && \
    tar -xf imagick*.tgz && \
    rm -f imagick*.tgz && \
    cd imagick* && \
    phpize && \
    ./configure && \
    make && \
    make install && \
    cd - && \
    rm -rf imagick* && \
    echo -en 'extension=imagick.so\n' >> /etc/php.d/25-imagick.ini && \
    # Install pecl-package mailparse
    dnf install -y re2c && \
    pecl install mailparse && \
    echo -en 'extension = mailparse.so\n' >> /etc/php.d/40-mailparse.ini && \
    # Install composer
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    # Clean dnf cache to have smaller image
    dnf clean all && \
    rm -rf /var/cache/dnf/

ADD devfs /