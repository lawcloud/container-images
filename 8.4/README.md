# Easily run the container 

```shell
podman build --no-cache -f Dockerfile -t i-php-83 . && \
    podman stop --ignore c-php-74 && \
    podman container rm --ignore c-php-83 && \
    podman run -dit --name c-php-83 i-php-83 && \
    podman exec c-php-83 rm -rf /app && \
    podman cp /home/int8/lawcloud-3 c-php-83:/app && \
    podman exec -it c-php-83 /bin/sh
```

# Run container if it is not staring
```shell

podman build --no-cache -f Dockerfile -t i-php-83 . && podman run --entrypoint /bin/sh --interactive --tty --name c-php-83 i-php-83

podman run --entrypoint /bin/sh --interactive --tty --name c-php-83 i-php-83

```